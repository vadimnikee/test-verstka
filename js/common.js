$(document).ready(function() {
	$.stellar({
		responsive:true
	});

	function slide(){
		$('.multiple-items').slick({
			dots: false,
			infinite: true,
			speed: 300,
			autoplaySpeed: 2000,
			slidesToShow: 4,
			slidesToScroll: 1,
			responsive: [
			{
				breakpoint: 986,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
					dots: false
				}
			},
			{
				breakpoint: 766,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,
					dots: false
				}
			}]
		});
	}
		slide();

	function fixedHeader(){
		var w_width = $(window).width();
		if ( w_width < 750){
			$("header .navbar.navbar-default").addClass("navbar-fixed-top");
		}
		else{
			$("header .navbar.navbar-default").removeClass("navbar-fixed-top");
		}
	}
	fixedHeader();
	$(window).resize(fixedHeader);

	function selectShow() {
	    jQuery('select').selectBox({ mobile: true });
	}
	selectShow();

});